from django import forms
from django.db import models
from django.db.models.fields import BLANK_CHOICE_DASH
import datetime

# New imports added for ClusterTaggableManager, TaggedItemBase, MultiFieldPanel

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.snippets.models import register_snippet
from wagtail.documents.edit_handlers import DocumentChooserPanel

class CvIndexPage(Page):
    intro = RichTextField(blank=True)
    location = models.CharField(max_length=255, blank=True)
    telephone = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    linkedin = models.CharField(max_length=255, blank=True)
    website = models.CharField(max_length=255, blank=True)
    degree = models.CharField(max_length=255, blank=True)
    degree_description = RichTextField(blank=True)
    skills = RichTextField(blank=True)
    scsf = RichTextField(blank=True)
    related_document = models.ForeignKey(
        'wagtaildocs.Document', blank=True, null=True,
         on_delete=models.SET_NULL, related_name='+'
    )

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super().get_context(request)
        cvpages = self.get_children().live()

        context['cvpages'] = cvpages
        return context

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("location"),
        FieldPanel("telephone"),
        FieldPanel("email"),
        FieldPanel("linkedin"),
        FieldPanel("website"),
        FieldPanel("degree"),
        FieldPanel("degree_description", classname="full"),
        FieldPanel("skills", classname="full"),
        FieldPanel("scsf", classname="full"),

        InlinePanel('gallery_images', label="Gallery images"),
        DocumentChooserPanel('related_document'),
        ]





class CvTagIndexPage(Page):


    def get_context(self, request):

        # Filter by tag
        tag = request.GET.get('tag')
        cvpages = CvPage.objects.filter(tags__name=tag)

        # Update template context
        context = super().get_context(request)
        context['cvpages'] = cvpages
        return context


class CvPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'CvPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )

@register_snippet
class CvCategory(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'cv categories'

class CvPage(Page):
    date = models.DateField("Post date")
    start_date = models.DateField("Start Date", default=datetime.date.today)
    stop_date = models.DateField("Stop Date", default=datetime.date.today, blank=True)
    location = models.CharField(max_length=250, blank=True)
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    details = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=CvPageTag, blank=True)
    categories = ParentalManyToManyField('cv.CvCategory', blank=True)


    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('start_date'),
            FieldPanel('stop_date'),
            FieldPanel('location'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="CV information"),
        FieldPanel('intro'),
        FieldPanel('body'),
        FieldPanel('details'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]


class CvPageGalleryImage(Orderable):
    page = ParentalKey(CvPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


class CvIndexPageGalleryImage(Orderable):
    page = ParentalKey(CvIndexPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]

